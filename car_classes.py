from math import pi, atan2, cos, sin, sqrt, atan
from random import random as rand
import random
import numpy as np
import copy
from numpy.linalg import norm

PI2 = pi*2

def line_intersection(A, B, C, D):
    CmP = C - A
    r = B - A
    s = D - C
    CmPxr = CmP[0] * r[1] - CmP[1] * r[0]
    CmPxs = CmP[0] * s[1] - CmP[1] * s[0]
    rxs = r[0] * s[1] - r[1] * s[0]
    if CmPxr == 0.0:
        #collinear
        q1 = norm(A - C)/norm(r)
        q2 = norm(A - D)/norm(r)
        if 0 > q1 or q1 > 1.0:
            q1 = 100
        if 0 > q2 or q2 > 1.0:
            q2 = 100
        if q1 != 100 or q2 != 100:
            return min(q1, q2)
        return -1
    if rxs == 0.0:
        #parallel
        return -1
    rxsr = 1.0/rxs
    t = CmPxs * rxsr
    u = CmPxr * rxsr
    if 0 <= t <= 1.0 and 0 <= u <=1.0:
        return t

    return -1

def dotp(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1]

def distsq(v1, v2):
    return (v1[0] - v2[0])**2 + (v1[1] - v2[1])**2

def line_segment_circle_collision_detection(center, radsq, linep1, linep2):
    ap = center - linep1
    ab = linep2 - linep1
    t = dotp(ap, ab)/dotp(ab, ab)
    #t = line[0] + t*ab
    if 0 <= t <= 1.0:
        if distsq(linep1 + t*ab, center) <= radsq:
            return True
        else:
            return False
    else:
        if distsq(center, linep1) <= radsq or distsq(center, linep2) <= radsq:
            return True
    return False

def get_projection_on_line(point, line_start, ab, dotab):
    ap = point - line_start
    t = dotp(ap, ab)/dotab
    return t

def circle_circle_collision_detection(center1, rad1, center2, rad2):
    return distsq(center1, center2) <= (rad1 + rad2)**2

def get_path_item(p_start, p_end):
    ab = p_end - p_start
    return (norm(ab), p_start, ab, dotp(ab, ab))

class Track:

    def __init__(self, wsize, chkpt_rad, filename=None):
        self.chkpt_rad = chkpt_rad
        self.walls = []
        self.checkpoints = []
        self.path = []
        if filename is not None:
            self.load(filename)
        else:
            self.add_wall([(0, 0), (wsize[0], 0), wsize, (0, wsize[1]), (0, 0)])

    def load(self, filename='track.txt'):
        self.walls = []
        self.checkpoints = []
        self.path = []
        f = open(filename, 'r')
        lines = f.read().split('\n')
        f.close()
        k = 0
        while lines[k] != 'e':
            self.walls.append([])
            while lines[k] != 'e':
                self.walls[-1].append(np.array([float(_) for _ in lines[k].split()]))
                k += 1
            k += 1
        k += 1
        while lines[k] != 'e':
            while lines[k] != 'e':
                self.add_checkpoint(*[float(_) for _ in lines[k].split()])
                k += 1

        self.chkpt_rad = float(lines[k + 1])

    def save(self, filename='track.txt'):
            f = open(filename, 'w')
            for w in self.walls:
                for p in w:
                    f.write('%0.2f %0.2f\n' % (p[0], p[1]))
                f.write('e\n')
            f.write('e\n')
            for c in self.checkpoints:
                f.write('%0.2f %0.2f\n' % (c[0], c[1]))
            f.write('e\n')
            f.write('%0.2f' % self.chkpt_rad)

            f.close()


    def add_wall(self, wall):
        self.walls.append([np.array(w) for w in wall])

    def add_checkpoint(self, x, y):
        for c in self.checkpoints:
            if circle_circle_collision_detection((c[0], c[1]), self.chkpt_rad, (x, y), self.chkpt_rad):
                return
        self.checkpoints.append(np.array([x, y]))
        self.path.append(get_path_item(self.checkpoints[-min(2, len(self.checkpoints))], self.checkpoints[-1]))
        self.path[0] = get_path_item(self.checkpoints[-1], self.checkpoints[0])

    def get_fitness_to(self, i):
        return self.path[i][0]


    def draw_checkpoint(self, ctx, x, y, color):
        ctx.set_source_rgb(0, 0, 0)
        ctx.new_sub_path()
        ctx.arc(x, y, 4, 0, PI2)
        ctx.fill()
        ctx.stroke()
        ctx.set_source_rgba(*color, 0.3)
        ctx.new_sub_path()
        ctx.arc(x, y, self.chkpt_rad, 0, PI2)
        ctx.fill()
        ctx.stroke()


    def draw(self, ctx, color, width, unvisited_ch_color, visited_ch_color, set_of_visited_checkpoints, draw_checkpoints=True):
        ctx.set_line_width(width)
        ctx.set_source_rgb(*color)
        for w in self.walls:
            if len(w) > 0:
                ctx.move_to(w[0][0], w[0][1])
                for p in w:
                    ctx.line_to(p[0], p[1])
                ctx.stroke()
        if draw_checkpoints:
            for i, p in enumerate(self.checkpoints):
                if i in set_of_visited_checkpoints:
                    self.draw_checkpoint(ctx, *p, visited_ch_color)
                else:
                    self.draw_checkpoint(ctx, *p, unvisited_ch_color)



    def check_circle_walls_intersection(self, center, radsq):
        for w in self.walls:
            for i in range(len(w) - 1):
                if line_segment_circle_collision_detection(center, radsq, w[i], w[i + 1]):
                    return True

    def check_if_agent_is_inside_next_checkpoint(self, pos, rad, last_checkpoint):
        next_c = (last_checkpoint + 1) % len(self.checkpoints)
        if circle_circle_collision_detection(self.checkpoints[next_c], self.chkpt_rad, pos, rad):
            return next_c

        return None


class LapTimer:

    def __init__(self, lap_size):
        self.lap_size = lap_size
        self.reset()

    def reset(self, lap_size=None):
        if lap_size is not None:
            self.lap_size = lap_size
        self.checkpoints_times = [0]
        self.timestamp = 1
        self.last_checkpoint_time = 0
        self.fitness = 0

    def update(self, track, agent):
        self.timestamp += 1
        cid = track.check_if_agent_is_inside_next_checkpoint(agent.pos, agent.rad, self.checkpoints_times[-1])
        if cid is None or cid == self.checkpoints_times[-1] or cid - self.checkpoints_times[-1] > 1:
            return
        if cid < self.checkpoints_times[-1] and not (cid == 0 and self.checkpoints_times[-1] == self.lap_size - 1):
            return
        #if we are here it means we hit a checkpoint
        self.checkpoints_times.append(cid)
        self.last_checkpoint_time = self.timestamp
        self.fitness += track.get_fitness_to(cid)

    def get_last_lap_checkpoints_number(self):
        offset = None
        if self.lap_size > 0:
            offset = len(self.checkpoints_times) % self.lap_size
            if offset == 0:
                offset = self.lap_size
        return offset


    def get_fitness(self, agent, track):
        path = track.path[(self.checkpoints_times[-1] + 1) % self.lap_size]
        return self.fitness + min(get_projection_on_line(agent.pos, path[1], path[2], path[3]), 1.0)*path[0]


class AgentInitParams:

    def __init__(self, rad, fov_angle, fov_line_step, fov_rad):
        self.rad = rad
        self.radsq = self.rad**2
        self.fov = (fov_angle*pi/180, fov_line_step*pi/180, int(fov_angle/fov_line_step) + 1, fov_rad)

class Agent:

    def __init__(self, params, pos, dir_a, angular_vel=0, vel=0):
        self.rad = params.rad
        self.radsq = params.rad**2
        self._angular_vel = angular_vel
        self._vel = vel
        self.fov = params.fov
        self.reset(pos, dir_a)

    def reset(self, pos, dir_a):
        self.pos = np.array(pos)
        self.dir_a = dir_a
        self.angular_vel = self._angular_vel
        self.vel = self._vel

    def get_fov_lines(self):
        return [(self.pos, self.pos + self.fov[3]*np.array([cos(self.dir_a - self.fov[0]*0.5 + angle*self.fov[1]), sin(self.dir_a - self.fov[0]*0.5 + angle*self.fov[1])])) for angle in range(self.fov[2])]

    def get_intersection_coeffs(self, walls):
        fov_lines = self.get_fov_lines()
        coeffs = [1]*len(fov_lines)
        for i, line in enumerate(fov_lines):
            for w in walls:
                for j in range(len(w) - 1):
                    c = line_intersection(*line, w[j], w[j + 1])
                    if c != -1 and c < coeffs[i]:
                        coeffs[i] = c
        return coeffs

    def draw(self, ctx, intersec_dic, width, agent_color, obstacle_color, clear_color):
        #intersec_dic is not a dic just a list
        ctx.set_source_rgba(*agent_color, 0.5)
        ctx.arc(*self.pos, self.rad, 0, PI2)
        ctx.fill()
        ctx.stroke()
        lines = self.get_fov_lines()

        ctx.set_line_width(width)
        for i, line in enumerate(lines):
            if intersec_dic[i] == 1:
                ctx.set_source_rgb(*clear_color)
                ctx.move_to(*self.pos)
                ctx.line_to(*line[1])
                ctx.stroke()
            else:
                ctx.set_source_rgb(*clear_color)
                ctx.move_to(*self.pos)
                l = (line[0] + (line[1] - line[0])*intersec_dic[i])
                ctx.line_to(*l)
                ctx.stroke()
                ctx.set_source_rgb(*obstacle_color)
                ctx.move_to(*l)
                ctx.line_to(*line[1])
                ctx.stroke()

    def update(self):
        self.pos[0] += cos(self.dir_a)*self.vel
        self.pos[1] += sin(self.dir_a)*self.vel
        self.dir_a += self.angular_vel

