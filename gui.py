from gi.repository import Gtk
from gi.repository import Gdk
import time
from gi.repository import GObject as gobject
from math import pi, atan2, cos, sin, sqrt, atan
from cairo_primitives import message
from random import random as rand
import random
import numpy as np
import copy
from numpy.linalg import norm
from car_classes import *
import cairo
import sys
sys.path.insert(0, '/home/saya')
from evo import network as nnet
from evo import alg

SIZE = 20
WIN_SIZE = 800, 600
PI2 = pi*2
to_rad = pi/180
to_deg = 180/pi

class Win(Gtk.Window):

    def __init__(self, size=(900, 900)):
        super(Win, self).__init__()

        self.init_ui(size)

    def init_ui(self, size):

        self.darea = Gtk.DrawingArea()
        self.darea.connect("draw", self.redraw)
        self.add(self.darea)

        self.set_title("Much balls.")
        self.resize(size[0], size[1])
        self.size = size
        self.set_position(Gtk.WindowPosition.CENTER)
        self.connect("delete-event", Gtk.main_quit)
        self.connect('destroy', lambda w: Gtk.main_quit())
        self.connect("motion_notify_event", self.motion_notify_event)
        self.connect('key_press_event', self.key_pressed)
        self.connect('key_release_event', self.key_released)
        self.connect('button_press_event', self.mouse_click)
        self.show_all()
        self.pause = True
        self.last = time.time()
        gobject.timeout_add(20, self.tick) # Go call tick every 50 whatsits.
        random.seed(11)
        np.random.seed(11)
        self.mx = 0
        self.my = 0
        self.lb_pressed = False
        self.rb_pressed = False
        self.draw_mode = False
        self.adding_checkpoints_mode = False
        self.tmp_line = None
        self.drawing_wall_idx = -1
        self.fov_angle = 80
        self.fov_line_step = 16
        self.fov_rad = 100

        self.default_chkpnt_rad = 100
        self.max_agent_vels = (4*pi/180, 8)#angular, linear
        self.track = Track(self.size, self.default_chkpnt_rad)
        self.agents = []
        self.reset_agents()

    def add_agent(self, agent_params, network_params, genes):
        net = nnet.construct_network(genes, *network_params)#weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
        ticks_needed = nnet.get_min_ticks(net)
        if ticks_needed == -1:
            #no meaning in testing return worst error possible
            print('Network for an agent you are trying to add is useless, please provide another set of genes')
            return
        lap_timer = LapTimer(len(self.track.checkpoints))
        agent = Agent(agent_params, *self.calc_pos_and_dir_a())
        self.agents.append((agent, lap_timer, net, ticks_needed, id(agent)))

    def find_agent_pos_in_list(self, agent_id):
        for i in range(len(self.agents)):
            if self.agents[i][-1] == agent_id:
                return i

    def calc_pos_and_dir_a(self):
        dira = 0
        if len(self.track.checkpoints) > 1:
            dira = self.track.checkpoints[1] - self.track.checkpoints[0]
            dira = atan2(dira[1], dira[0])
        pos = (np.array(self.size)*0.5 if len(self.track.checkpoints) == 0 else self.track.checkpoints[0]).copy()
        return pos, dira

    def remove_agent(self, agent_id):
        del self.agents[self.find_agent_pos_in_list(agent_id)]

    def reset_agent(self, pos_in_array, agent_id=None):
        if pos_in_array is None:
            pos_in_array = self.find_agent_pos_in_list(agent_id)
        self.agents[pos_in_array][0].reset(*self.calc_pos_and_dir_a())
        self.agents[pos_in_array][1].reset()
        self.agents[pos_in_array][2].reinit()

    def reset_agents(self):
        self.timestamp = 0
        for i in range(len(self.agents)):
            self.reset_agent(i)

    def update_agents(self):
        for i in range(len(self.agents)):
            self.update_agent(i)

    def update_agent(self, pos_in_array):
        a = self.agents[pos_in_array]
        input_vec = a[0].get_intersection_coeffs(self.track.walls)#theyare betwee 0..1 already
        control_vec = a[2].process_input(input_vec, a[3])
        a[0].angular_vel = control_vec[0]
        a[0].vel = control_vec[1]
        a[0].update()
        a[1].update(self.track, a[0])

    def motion_notify_event(self, widget, event):
        self.mx, self.my, = event.x, event.y#self.size[1] - event.y
        #print('q')

    def mouse_click(self, widget, event):
        if not self.draw_mode:
            return

        if event.button == 1:
            if self.lb_pressed and not self.adding_checkpoints_mode:
                if self.drawing_wall_idx == -1:
                    for i in range(len(self.track.walls)):
                        delta = np.absolute(self.track.walls[i][-1] - np.array([event.x, event.y]))
                        if delta[0] < 5 and delta[1] < 5:
                            self.drawing_wall_idx = i
                            break
                    if self.drawing_wall_idx == -1:
                        self.drawing_wall_idx = len(self.track.walls)
                        self.track.add_wall([])
                else:
                    delta = np.absolute(self.track.walls[self.drawing_wall_idx][-1] - np.array([event.x, event.y]))
                    if delta[0] < 5 and delta[1] < 5:
                        self.drawing_wall_idx = -1

                if self.drawing_wall_idx != -1:
                    self.track.walls[self.drawing_wall_idx].append(np.array([event.x, event.y]))
                    self.tmp_line = [self.track.walls[self.drawing_wall_idx][-1], self.track.walls[self.drawing_wall_idx][-1]]
                else:
                    self.tmp_line = None
            elif self.lb_pressed and self.adding_checkpoints_mode:
                self.track.add_checkpoint(event.x, event.y)



            self.lb_pressed = not self.lb_pressed

    def key_pressed(self, event, event2):
        if event2.keyval == ord('t'):
            self.track = Track(self.size, self.default_chkpnt_rad)
            self.drawing_wall_idx = -1
            self.tmp_line = None

        if event2.keyval == ord('r'):
            self.reset_agents()

        if event2.keyval == ord('p') and not self.draw_mode:
            self.pause = not self.pause

        if event2.keyval == ord('u'):
            self.draw_mode = not self.draw_mode
            self.drawing_wall_idx = -1
            self.adding_checkpoints_mode = False
            self.tmp_line = None
            self.pause = self.draw_mode

        if event2.keyval == ord('c') and self.draw_mode:
            self.adding_checkpoints_mode = not self.adding_checkpoints_mode
            if self.adding_checkpoints_mode:
                self.tmp_line = None

        if event2.keyval == ord('s'):
            self.track.save()

        if event2.keyval == ord('l'):
            self.track.load()
            import pickle
            q = pickle.load(open('7778.85_1000_6_2_10_10_20_228_10.dat', 'rb'))
            dna = q[0][0]
            agent_game_over_size = 20
            fov_angle = 80
            fov_line_step = 16
            fov_rad = 100
            ch_rad = 100
            max_agent_vels = (4*pi/180, 8)#angular, linear
            win_size = (900, 900)
            a_params = AgentInitParams(agent_game_over_size, fov_angle, fov_line_step, fov_rad)

            weights_scaler = 10
            #input_dim = 2
            input_dim = int(fov_angle/fov_line_step) + 1

            output_dim = 2

            hidden_dim = 10
            output_ranges = [(-max_agent_vels[0], max_agent_vels[0]), (-max_agent_vels[1], max_agent_vels[1])]

            template_genes = [1170499, 12524492, 36365114, 72542922, 73143190, 100640060, 108111773, 131497505, 146268432, 164887265, 165186302, 190095751, 224240203, 242425949, 267378997, 275165289, 281311136, 281592091, 287842182, 293229220, 306117130, 340875610, 398813920, 423788256, 452697409, 479014108, 483890759, 491886318, 492412848, 544921481, 545339927, 563890773, 567287571, 577943680, 578892547, 590013626, 596066144, 603768397, 630076539, 641974504, 645868022, 665565332, 688689542, 694112144, 696035144, 723070337, 735464173, 777048066, 784396062, 799928108, 813110337, 830502584, 900215299, 911447241, 923216947, 927660151, 992160300, 999185745, 1009483092, 1021810743, 1023631220, 1067157536, 1073253199, 1132165610, 1134357306, 1140597833, 1158499822, 1172331233, 1185725910, 1268914334, 1281738880, 1291540892, 1309928074, 1359746658, 1425423434, 1427836512, 1448512869, 1464205839, 1476652231, 1478500741, 1483061416, 1507354098, 1529757754, 1530083513, 1579777873, 1593012880, 1601758698, 1602290791, 1605652597, 1621607633, 1624832766, 1646143061, 1647615409, 1652889015, 1666659691, 1746983571, 1754007372, 1754453011, 1762730544, 1765771413, 1766015606, 1798847411, 1806101430, 1814848108, 1816795417, 1841576202, 1874124862, 1879752339, 1889731719, 1902734705, 1902891647, 1908612432, 1918588362, 1997732439, 2074379827, 2079617401, 2106916305, 2124247567, 2131093059, 2175005052, 2180251563, 2260814758, 2268960153, 2322317539, 2336120533, 2336413928, 2349533189, 2393464792, 2403671433, 2421242894, 2440336074, 2463671732, 2474030424, 2481049072, 2504039185, 2517598674, 2548072441, 2552403379, 2557393628, 2559774432, 2563311262, 2566596402, 2586470481, 2620275550, 2628276619, 2696648268, 2782676778, 2798937751, 2801191826, 2807289102, 2834527856, 2837314234, 2845814799, 2854673503, 2855513478, 2872711120, 2911198829, 2936977779, 2946012427, 2949856480, 3029376209, 3049202944, 3049216864, 3052657644, 3064693501, 3095806364, 3106263367, 3107329966, 3108785464, 3116184519, 3116659263, 3124311729, 3149514352, 3149913886, 3160082949, 3203894600, 3210840126, 3220592128, 3239076767, 3242647073, 3261153549, 3281840919, 3293724716, 3325885705, 3368883132, 3375225123, 3409107420, 3488784292, 3492905482, 3507278180, 3545974644, 3547996249, 3556617220, 3585778566, 3601294703, 3609031332, 3615070504, 3619291089, 3619875478, 3625493031, 3709864742, 3721553269, 3730397362, 3739265752, 3746897078, 3761293967, 3769327460, 3826027864, 3844091522, 3848947144, 3884291238, 3885505456, 3928126767, 3967508019, 3980613797, 3987859473, 4010864849, 4016348557, 4059244524, 4078801541, 4131063053, 4131615088, 4140503229, 4161422817, 4213036534, 4222042472, 4238656494, 4282038408]
            template_genes_number = nnet.genes_per_neuron(input_dim, output_dim, hidden_dim)*(hidden_dim + output_dim)
            step = 2**32/(template_genes_number + 1)
            genes = list(list(zip(*sorted(dna.get_gene_values(template_genes, step).items(), key=lambda x:x[0])))[1])

            net_params = (weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
            self.add_agent(a_params, net_params, genes)
            #self.add_agent(a_params, net_params, genes)
            #self.add_agent(a_params, net_params, genes)
            #self.add_agent(a_params, net_params, genes)
            #self.add_agent(a_params, net_params, genes)
            self.reset_agents()
            #self.agents[1][0].dir_a -= 0.6
            #self.agents[2][0].dir_a -= 0.4
            #self.agents[3][0].dir_a += 0.3
            #self.agents[4][0].dir_a += 0.4


        #WARNING ALL THAT BELOW WORKS ONLY WHEN PAUSE IS FALSE
        if self.pause:
            return

        if event2.keyval == ord('w'):
            self.agent.vel = self.max_agent_vels[1]
        elif event2.keyval == ord('s'):
            self.agent.vel = -self.max_agent_vels[1]

        if event2.keyval == ord('a'):
            self.agent.angular_vel = -self.max_agent_vels[0]
        elif event2.keyval == ord('d'):
            self.agent.angular_vel = self.max_agent_vels[0]

    def key_released(self, event, event2):
        #WARNING ALL THAT BELOW WORKS ONLY WHEN PAUSE IS FALSE
        if self.pause:
            return
        if event2.keyval == ord('w'):
            self.agent.vel = 0
        elif event2.keyval == ord('s'):
            self.agent.vel = 0

        if event2.keyval == ord('a'):
            self.agent.angular_vel = 0
        elif event2.keyval == ord('d'):
            self.agent.angular_vel = 0

    def redraw(self, wid, ctx):
        ctx.set_source_rgb(1, 1, 1) # blue
        ctx.rectangle(0, 0, self.size[0], self.size[1])
        ctx.fill()

        message(ctx, (20, 40), 'fps: %0.1f' % (1/(time.time() - self.last)))
        if self.pause and not self.draw_mode:
            message(ctx, (self.size[0] - 150, 40), 'Paused')
        elif self.draw_mode:
            message(ctx, (self.size[0] - 220, 40), 'Draw mode')
            if self.adding_checkpoints_mode:
                message(ctx, (self.size[0] - 220, 80), 'chkpt')

        best = None
        if len(self.agents) > 0:
            fv = [a[1].get_fitness(a[0], self.track) for a in self.agents]
            fmaxv = fv[0]
            best = self.agents[0]
            for i in range(len(fv)):
                if fv[i] > fmaxv:
                    best = self.agents[i]
                    fmaxv = fv[i]
        set_of_visited_checkpoints = set()
        if best:
            if best and best[1].lap_size > 0:
                message(ctx, (20, 80), 'f: %0.2f' % (best[1].get_fitness(best[0], self.track)))

            offset = best[1].get_last_lap_checkpoints_number()
            if offset is not None:
                set_of_visited_checkpoints = set(best[1].checkpoints_times[-offset:])

        self.track.draw(ctx, (0, 0, 0), 8, (0, 1, 0), (0, 0, 1), set_of_visited_checkpoints)

        self.last = time.time()

        self.draw(ctx)

        if self.pause:
            return

        self.update_agents()

    def draw(self, ctx):
        #ctx.save()
        #ctx.scale(1, -1)
        #ctx.translate(0, -self.size[1])

        for a in self.agents:
            a[0].draw(ctx, a[0].get_intersection_coeffs(self.track.walls), 2,
                (1, 0, 0) if self.track.check_circle_walls_intersection(a[0].pos, a[0].radsq) else (0, 0, 0), (1, 0, 0), (0, 1, 0))

        if self.adding_checkpoints_mode:
            self.track.draw_checkpoint(ctx, *self.darea.get_pointer(), (0, 1, 0))
        if self.tmp_line is not None:
            self.tmp_line[1] = self.darea.get_pointer()
            ctx.set_line_width(8)
            ctx.set_source_rgb(0, 0, 0)
            ctx.move_to(*self.tmp_line[0])
            ctx.line_to(*self.tmp_line[1])
            ctx.stroke()


        #ctx.restore()


    def tick(self):
        self.darea.queue_draw()
        #IMPORTANT TO RETURN TRUE TO TIMEOUT_ADD
        return True

def draw_node(ctx, x, y, node_rad, node_c):
    ctx.set_line_width(node_rad*0.1)
    ctx.set_source_rgb(0, 0, 0)
    ctx.new_sub_path()
    ctx.arc(x, y, node_rad, 0, PI2)
    ctx.stroke_preserve()
    ctx.set_source_rgb(*node_c)
    ctx.fill()
    ctx.stroke()

def draw_edge(ctx, p1, p2, max_width, color, alpha):
    #add arrows!
    #from p1 to p2
    ctx.set_line_width(max_width*alpha)
    ctx.set_source_rgba(*color, alpha)
    ctx.move_to(*p1)
    ctx.line_to(*p2)
    ctx.stroke()

def create_network_picture(net, node_rad, nodes_c, max_edge_width, weights_scaler, edge_pos_c, edge_neg_c, size, scale):
    """ nodes_c of size 3 - color of input, hidden and output neurons """
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, *size)
    ctx = cairo.Context(surface)
    ctx.set_source_rgb(1, 1, 1)
    ctx.rectangle(0, 0, *size)
    ctx.fill()
    ctx.stroke()
    ctx.scale(*scale)
    x_spacing = node_rad*2 + node_rad#first must always be and second can be varied
    x = x_spacing - node_rad

    spacing_in_rads = 5*pi/180
    max_rad_L = int(round((min(size[0], size[1]) - 2*x_spacing + node_rad)/2))
    L = max_rad_L
    """
    for l in range(node_rad, max_rad_L, node_rad):
        a = atan(node_rad/l)
        if (a*2 + spacing_in_rads)*net.hidden_dim <= PI2:
            L = l
            break
    """

    y_spacing = size[1]/(net.input_dim + 1)#+1 is not bias if adding bias need +2. just need this one
    y = y_spacing

    pos = {}

    for i in range(net.input_dim):
        pos[net.stationary_neurons[i].id] = (x, y)
        y += y_spacing
    x = size[0]*0.5
    y = size[1]*0.5
    for i in range(net.hidden_dim):
        angle = i*PI2/net.hidden_dim + pi
        xx = x + cos(angle)*L
        yy = y + sin(angle)*L
        pos[net.neurons[i + net.output_dim].id] = (xx, yy)
    x = size[0] - (x_spacing - node_rad)
    y_spacing = size[1]/(net.output_dim + 1)#+1 is not bias if adding bias need +2. just need this one
    y = y_spacing
    for i in range(net.output_dim):
        pos[net.neurons[i].id] = (x, y)
        y += y_spacing

    for n in net.neurons:
        for nn, w in n.incoming_connections:
            if nn.id == net.input_dim:#bias
                continue
            draw_edge(ctx, pos[nn.id], pos[n.id], max_edge_width, edge_pos_c if w > 0 else edge_neg_c, abs(w/weights_scaler))

    ctx.set_font_size(20)
    for k, v in pos.items():
        x, y = v
        idd = k
        if k > net.input_dim + net.output_dim: #> because bias adds +1
            draw_node(ctx, x, y, node_rad, nodes_c[1])
            idd -= net.output_dim
        elif k < net.input_dim:
            draw_node(ctx, x, y, node_rad, nodes_c[0])
        else:
            draw_node(ctx, x, y, node_rad, nodes_c[2])
            idd += net.hidden_dim
        ctx.move_to(x - node_rad*0.4, y + node_rad*0.3)
        #TODO calculate pos
        ctx.set_source_rgb(0, 0, 0)
        ctx.show_text(str(idd))

    return surface



#genes = [0.23138177160622453, 0.1253211002113529, 0.3300183314930305, 0.39223619610804422, 0.39062029142138605, 0.79542202134916051, 0.9927526206099675, 0.85428234915250312, None, None, 0.24879171412280957, 0.35090351430557437, None, None, 0.077880894414185386, 1.0, 0.0093313080734043468, None, 0.26537599038802756, 0.26841360435556416, 1.0, 1.0, 0.96530356272327189, 0.26010150482657496, 0.26700139729713801, 0.27078747291367261, 0.44913318277291192, None]

def main():
    import imp
    nnet = imp.load_module('network', *imp.find_module('network', ['/home/saya/evo/']))
    weights_scaler = 10
    input_dim = 2
    output_dim = 1
    hidden_dim = 1
    output_ranges = [(0, 1)]
    genes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 ,0.8, 0.9, 1.0]
    net = nnet.construct_network(genes, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)

    surface = create_network_picture(net, 20, ((0, 1, 0), (1, 1, 0), (0, 1, 1)), 20, weights_scaler, (1, 0, 0), (0, 0, 1), (800, 600), (1, 1))
    surface.write_to_png ("net.png")

    app = Win()
    ##Gtk.gtk_window_set_modal(app, True)
    Gtk.main()

if __name__ == '__main__':
    main()

