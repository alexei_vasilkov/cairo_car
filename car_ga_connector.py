#import imp
import numpy as np
import random
from car_classes import *

#FOR DEBUG ONLY
rseed = 10
random.seed(rseed)
np.random.seed(rseed)

steps_per_run = 1000
agent_game_over_size = 20
fov_angle = 80
fov_line_step = 16
fov_rad = 100
ch_rad = 100
a_params = AgentInitParams(agent_game_over_size, fov_angle, fov_line_step, fov_rad)
max_agent_vels = (4*pi/180, 8)#angular, linear
win_size = (900, 900)
trackk = Track(win_size, ch_rad, filename='track.txt')


#nnet = imp.load_module('network', *imp.find_module('network', ['/home/saya/evo/']))
#alg = imp.load_module('alg', *imp.find_module('alg', ['/home/saya/evo/']))
import sys
sys.path.insert(0, '/home/saya')
from evo import network as nnet
from evo import alg
weights_scaler = 10
#input_dim = 2
input_dim = int(fov_angle/fov_line_step) + 1

output_dim = 2

hidden_dim = 10
output_ranges = [(-max_agent_vels[0], max_agent_vels[0]), (-max_agent_vels[1], max_agent_vels[1])]

#GA params
pop_size = 20
template_genes_number = nnet.genes_per_neuron(input_dim, output_dim, hidden_dim)*(hidden_dim + output_dim)
step = 2**32/(template_genes_number + 1)
template_genes = sorted(np.random.random_integers(0, 2**32, template_genes_number))
print(template_genes)

dna_size_in_genes = template_genes_number
max_dna_size_in_genes = dna_size_in_genes*20

def f_f_net(dna):
    global trackk
    genes = list(list(zip(*sorted(dna.get_gene_values(template_genes, step).items(), key=lambda x:x[0])))[1])
    net = nnet.construct_network(genes, weights_scaler, input_dim, output_dim, hidden_dim, output_ranges)
    ticks_needed = nnet.get_min_ticks(net)
    if ticks_needed == -1:
        #no meaning in testing return worst error possible
        ticks_needed = 0
        return 0 #maybe it can output at least something good?
    #ticks_needed += 1#needed.
    #TODO find a good min tick
    f = 0

    dira = 0
    if len(trackk.checkpoints) > 1:
        dira = trackk.checkpoints[1] - trackk.checkpoints[0]
        dira = atan2(dira[1], dira[0])
    agent = Agent(a_params, trackk.checkpoints[0], dira)
    lap_timer = LapTimer(len(trackk.checkpoints))
    #without reinitialization of the network!
    for i in range(steps_per_run):
        input_vec = agent.get_intersection_coeffs(trackk.walls)#theyare betwee 0..1 already
        control_vec = net.process_input(input_vec, ticks_needed)
        agent.angular_vel = control_vec[0]
        agent.vel = control_vec[1]
        agent.update()
        if trackk.check_circle_walls_intersection(agent.pos, agent.radsq):
            return lap_timer.get_fitness(agent, trackk)
        lap_timer.update(trackk, agent)

    return lap_timer.get_fitness(agent, trackk)


m_rates = alg.MutationRates(max_dna_size_in_genes, 0.01, 0.8, 5, 0.05, 640, 0.01, 3, 0.01, 2)
g_obj = alg.GeneStructureObj(4, 32, 0.7, 32, 0.4, 3)#TODO ADD type and boolean percents not just 50/50
orgs = [alg.DNA(dna_size_in_genes, g_obj) for _ in range(pop_size)]
ea = alg.EvoAlg(orgs, pop_size, 0.1, f_f_net, 100, m_rates, None, True, int(pop_size*0.1), 4, True)
ea.run()
print(ea.best[-1][1])
import pickle
pickle.dump((ea.best[-1], m_rates, g_obj), open('%0.2f_%d_%d_%d_%d_%d_%d_%d_%d.dat' % (ea.best[-1][1], steps_per_run, input_dim, output_dim, hidden_dim, weights_scaler, pop_size, dna_size_in_genes, rseed),
            'wb'))
o = ea.best[-1][0]


